<?php

namespace App\Http\Controllers;

use App\Models\Pen;
use App\Http\Requests\StorePenRequest;
use App\Http\Requests\UpdatePenRequest;
use App\Models\User;
use Illuminate\Http\Request;

class PenController extends Controller
{
    public function logint(Request $request){

     /** @var User $user  */
    $user = User::query()->where('email', '=', $request->get('email'))->first();
    if($user){
        $check = password_verify($request->get('password'), $user->password);
        if($check){
            $token = $user->createToken('email');
            return ['token' => $token->plainTextToken];
        }else{
            return response()->json(['msg' => 'shets']);
        }
    }else{
        return response()->json(['msg' => 'shets']);
    }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pens = Pen::all();
        return response()->json($pens);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePenRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePenRequest $request)
    {
        $pen = Pen::create($request->validated());
        return response()->json($pen);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pen  $pen
     * @return \Illuminate\Http\Response
     */
    public function show(Pen $pen)
    {
        return response()->json($pen);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePenRequest  $request
     * @param  \App\Models\Pen  $pen
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePenRequest $request, Pen $pen)
    {
      $pen =  $pen->update($request->validated());
      return response()->json($pen);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pen  $pen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pen $pen)
    {
        $p = $pen->delete();
        if($p){
            return response()->json(['message'=> 'deleted successfully']);
        }
        return response()->json(['message'=> 'error']);
    }
}
